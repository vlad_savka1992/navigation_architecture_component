package com.example.navigationarchitecturecomponent.ui.login

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.ActivityNavigator
import androidx.navigation.Navigation
import com.example.navigationarchitecturecomponent.MainActivity
import com.example.navigationarchitecturecomponent.R

class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.login_fragment, container, false)
        val toMainScreenButton = view.findViewById(R.id.btnToMain) as Button

        toMainScreenButton.setOnClickListener {
         //  startActivity(Intent(it.context,MainActivity::class.java))

            Navigation.findNavController(it).navigate(R.id.action_loginFragment2_to_MainAcivity)
        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
