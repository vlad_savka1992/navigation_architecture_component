package com.example.navigationarchitecturecomponent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.navigationarchitecturecomponent.ui.login.LoginFragment

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        supportActionBar!!.title="LoginActivity"
        Toast.makeText(this,"LoginActivity", Toast.LENGTH_LONG).show()
    }

}
